#[derive(Debug)]
pub struct Matrix<T> {
    nrows: usize,
    ncols: usize,
    data: Vec<T>,
}


impl<T> Matrix<T>
where
    T: Clone,
{

    pub fn new(nrows:usize, ncols:usize, init:T) -> Self {
        Matrix {
            nrows, ncols,
            data: vec![ init ; nrows*ncols ]
        }
    }

    pub fn nrows(&self) -> usize {
        self.nrows
    }

    pub fn ncols(&self) -> usize {
        self.ncols
    }

    pub fn as_slice(&self) -> &[T] {
        self.data.as_slice()
    }

    pub fn as_mut_slice(&mut self) -> &mut [T] {
        self.data.as_mut_slice()
    }

}


// One-dimensional iterators

pub struct MatrixIter1DCol<'a, T> {
    iter: std::slice::Iter<'a, T>,
}

impl<'a, T> Iterator for MatrixIter1DCol<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}


pub struct MatrixIter1DColMut<'a, T> {
    iter: std::slice::IterMut<'a, T>,
}


impl<'a, T> Iterator for MatrixIter1DColMut<'a, T> {
    type Item = &'a mut T;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}

impl<T> Matrix<T> {
    pub fn iter_1d_col(&self) -> MatrixIter1DCol<'_, T> {
        MatrixIter1DCol {
            iter: self.data.iter(),
        }
    }

    pub fn iter_1d_col_mut(&mut self) -> MatrixIter1DColMut<'_, T> {
        MatrixIter1DColMut {
            iter: self.data.iter_mut(),
        }
    }
}



// Row iterators

pub struct MatrixIterRow<'a, T> {
    iter: std::iter::StepBy<std::slice::Iter<'a, T>>,
}

impl<'a, T> Iterator for MatrixIterRow<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}


pub struct MatrixIterRowMut<'a, T> {
    iter: std::iter::StepBy<std::slice::IterMut<'a, T>>,
}


impl<'a, T> Iterator for MatrixIterRowMut<'a, T> {
    type Item = &'a mut T;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}


impl<T> Matrix<T> {
    pub fn iter_row(&self, row: usize) -> MatrixIterRow<'_, T> {
        MatrixIterRow {
            iter: self.data[row..].iter().step_by(self.nrows),
        }
    }

    pub fn iter_row_mut(&mut self, row: usize) -> MatrixIterRowMut<'_, T> {
        MatrixIterRowMut {
            iter: self.data[row..].iter_mut().step_by(self.nrows),
        }
    }
}


// Column iterators

pub struct MatrixIterCol<'a, T> {
    iter: std::slice::Iter<'a, T>,
}

impl<'a, T> Iterator for MatrixIterCol<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}


pub struct MatrixIterColMut<'a, T> {
    iter: std::slice::IterMut<'a, T>,
}


impl<'a, T> Iterator for MatrixIterColMut<'a, T> {
    type Item = &'a mut T;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}


impl<T> Matrix<T> {
    pub fn iter_col(&self, col: usize) -> MatrixIterCol<'_, T> {
        let nrows = self.nrows;
        let start = col*nrows;
        let end = start+nrows;
        MatrixIterCol {
            iter: self.data[start..end].iter(),
        }
    }

    pub fn iter_col_mut(&mut self, col: usize) -> MatrixIterColMut<'_, T> {
        let nrows = self.nrows;
        let start = col*nrows;
        let end = start+nrows;
        MatrixIterColMut {
            iter: self.data[start..end].iter_mut(),
        }
    }
}



// One-dimensional iterator by row

pub struct MatrixIter1DRow<'a, T> {
    mat: &'a Matrix<T>,
    iter: MatrixIterRow<'a, T>,
    row: std::ops::Range <usize>,
}

impl<'a, T> Iterator for MatrixIter1DRow<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(v) = self.iter.next() {
            Some(v)
        } else if let Some(new_row) = self.row.next() {
            self.iter = self.mat.iter_row(new_row);
            self.iter.next()
        } else {
            None
        }
    }
}

pub struct MatrixIter1DRowMut<'a, T> {
    mat: &'a mut Matrix<T>,
    iter: MatrixIterRowMut<'a, T>,
    row: std::ops::Range <usize>,
}


/*
impl<'a, T> Iterator for MatrixIter1DRowMut<'a, T> {
    type Item = &'a mut T;

    fn next(&mut self) -> Option<Self::Item> {
        if let v = self.iter.next() {
            v,
            None => {
                let new_row = self.row.next();
                match new_row {
                    None => None,
                    Some(i) => {
                        self.iter = self.mat.iter_row_mut(i);
                        self.iter.next()
                    }
                }
            }
        }
    }
}
*/

impl<T> Matrix<T> {
    pub fn iter_1d_row(&self) -> MatrixIter1DRow<'_, T> {
        MatrixIter1DRow {
            mat: self,
            iter: self.iter_row(0),
            row: 1..self.nrows,
        }
    }

    /*
    pub fn iter_1d_row_mut(&mut self) -> MatrixIter1DRowMut<'_, T> {
        MatrixIter1DRowMut {
            mat: self,
            iter: self.iter_row_mut(0),
            row: 1..self.nrows,
        }
    }
    */
}




mod matrix;
use matrix::Matrix;
    
fn main() {
    let mut mat = Matrix::<_>::new(4,3, 0.0f64);
    println!("nrows : {}", mat.nrows());
    println!("ncols : {}", mat.ncols());
    let mut k=0;
    for x in mat.iter_1d_col_mut() {
        *x = k as f64;
        k += 1;
    }

    println!("{:#?}", mat);
    for i in mat.iter_1d_col() {
        print!("{} ", *i);
    }
    println!("");

    println!("");
    for r in 0..mat.nrows() {
        for i in mat.iter_row(r) {
            print!("{} ", *i);
        }
        println!("");
    }

    println!("");
    for r in 0..mat.ncols() {
        for i in mat.iter_col(r) {
            print!("{} ", *i);
        }
        println!("");
    }
    println!("");

    for i in mat.iter_1d_row() {
        print!("{} ", *i);
    }
    println!("");
}
